#!/bin/bash
# Grab the username of the person to be added to sudoers
echo "Please input your username:"
read USER
# Update package lists
apt-get update
# Make our custom motd file
touch /etc/profile.d/motd.sh
#Put in our favorite message :)
printf "figlet -f slant 'WELCOME ${USER}'\nfortune | cowsay\n" > /etc/profile.d/motd.sh
# Be sure to run 'sh ./install_suite.sh' with super user priviledges to run this
# Developer tools -- assuming I grabbed git and used it to get this gist
apt-get install emacs build-essential gdb autoconf automake manpages-dev libsdl2-dev \
# Tools
sudo aptitude synaptic ranger \
# Video Requirements, Comment out if not wanted
## This setup is for intel chips and openbox as the WM
## Might want to look up what video driver is needed for nvida or radeon cards
## sudo apt-cache search [option] is how to search for other window managers
xorg mesa-utils xserver-xorg-video-intel \
# Window Compositor
#compton \
# GTK+
gtk+2.0 gtk+3.0 \
# GTK Theme Manager
lxappearance \
# Favorite GTK Theme
numix-gtk-theme \
# Display Manager
#wdm
## Window Managers
### Grab i3-gaps from it's repo
## OpenBox ~7mb
#openbox obconf obmenu \
# FVWM ~13mb
#fvwm \
# Sound Requirements
alsa-utils alsamixergui libasound2 \
## Music Players
#cmus \
#mpd ncmpcpp \
# Web applications
links2 iceweasel chromium \
# Games
chocolate-doom mednafen dosbox \
# Fun stuff
fortune cowsay figlet
# Setup sudo
usermod -aG sudo $USER
# Setup sound
alsactl init
# Ending instructions
echo "If you are running a laptop, run 'powertop --calibrate' to extend your battery life"
echo "After all other files have been setup, run /sbin/shutdown -r now"
