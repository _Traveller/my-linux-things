# My personal collection of favorite bash aliases
# Navigation commands
alias la='ls -A'
alias ll='ls -l'
alias cls='clear'
alias dir='ls -lA'

# System comands
alias shutdown='/sbin/shutdown now'
